Closes #XX

#### Before submitting MR:
* [ ] Merge the latest master / epic / release into your branch if there are conflicts
* [ ] Replace `#XX` above with a reference to your issue so it can be closed automatically
* [ ] Answer the questionnaire below to help the reviewer
* [ ] Move the issue to the `Needs Review` state within the sprint board

#### Questionnaire

##### 1. Which environment should be used for testing? Does the reviewer need to ask BE devs to deploy a certain BE branch?

https://testloon.de

##### 2. Is there a special setup needed for testing? Special account or channel to use, features you need to activate, mocks for BE requests, custom channel manifest you need to create

No special setup needed

##### 3. Did you create any followups (e.g. for tests, or missing functionality)?

Not needed

##### 4. Additional notes (e.g. branch should be merged together with BE branch)

No
